package org.abbtech.practice.exercise.controller;


import org.abbtech.practice.exercise.dto.CalculationRespDto;
import org.abbtech.practice.exercise.service.ApplicationService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/calculation")
public class CalculationController {


   private final ApplicationService applicationService;

    public CalculationController(ApplicationService applicationService) {
        this.applicationService = applicationService;
    }


    @PostMapping("/add")
    public void addCal(@RequestParam(name = "x")String x,@RequestParam(name = "y") String y){
        System.out.println(applicationService.addition(Integer.parseInt(x),Integer.parseInt(y)));

    }

    @PostMapping("/multiply")
    public double calculationRespDto (@RequestBody CalculationRespDto calculationRespDto){
        return applicationService.multiply(calculationRespDto.a,calculationRespDto.b);
    };


}
