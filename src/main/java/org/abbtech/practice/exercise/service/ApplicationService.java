package org.abbtech.practice.exercise.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApplicationService {

    @Autowired
    private final CalculationService calculationService;

    public ApplicationService(CalculationService calculationService) {
        this.calculationService = calculationService;
    }

    public int multiply(int a, int b){
        return calculationService.multiply(a, b);
    };

    public double divide(double a,double b){
        if (calculationService.division(a,b)%2!=0)
            throw new ArithmeticException();

       return calculationService.division(a,b);
    }

    public int addition(int a,int b){
        if (calculationService.addition(a,b)<0)
            throw new ArithmeticException();


        System.out.println(a+b);
        return calculationService.addition(a,b);
    }

    public int subtract(int a,int b){
        return calculationService.subtract(a,b);
    }

}
