package org.abbtech.practice.exercise.service;

import org.springframework.stereotype.Service;

@Service
public interface CalculationService {
  int multiply(int a,int b);
  int subtract(int a,int b);
  int addition(int a,int b);
  double division(double a, double b);
}
