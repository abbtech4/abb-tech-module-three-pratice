package org.abbtech.practice;

import org.abbtech.practice.tasks.task19.model.Student;
import org.abbtech.practice.tasks.task19.model.StudentIDCard;
import org.abbtech.practice.tasks.task19.repository.StudentRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.Date;

@SpringBootApplication

public class PracticeApplication {


	public static void main(String[] args) {
		SpringApplication.run(PracticeApplication.class, args);



	}


}
