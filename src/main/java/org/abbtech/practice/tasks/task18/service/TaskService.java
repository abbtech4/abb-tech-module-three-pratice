package org.abbtech.practice.tasks.task18.service;

import org.abbtech.practice.tasks.task18.dto.TaskDto;
import org.abbtech.practice.tasks.task18.exception.NoSuchTaskException;
import org.abbtech.practice.tasks.task18.repository.TaskRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService {
    private final TaskRepository taskRepository;


    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<TaskDto> getAllTasks(){

        return taskRepository.getAllTasks().orElseThrow(()->new NoSuchTaskException("No such tasks"));

    }

    public String createTask(List<TaskDto> taskDtos){
        taskRepository.createTask(taskDtos);
        return "success";
    };

    public void saveTask(TaskDto taskDto){
        taskRepository.saveTask(taskDto);
    }
    public TaskDto getTaskById(Long id){

        TaskDto taskDto=taskRepository.getTaskById(id).orElseThrow(()-> new NoSuchTaskException("No task found"));
        return taskDto;
    }

    public void updateTask(Long id,TaskDto taskDto){
        taskRepository.updateTask(id,taskDto);
    }

    public void deleteMultipleTasks(List<Long> ids){
        taskRepository.deleteMultipleTasks(ids);
    }

    public void deleteTask(Long id){
        taskRepository.deleteTask(id);
    }

}
