package org.abbtech.practice.tasks.task18.dto;

public class TaskDto {
    Long id;

    String taskName;

    public Long getId() {
        return id;
    }

    public TaskDto(Long id, String taskName) {
        this.id = id;
        this.taskName = taskName;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTask_name(String taskName) {
        this.taskName = taskName;
    }
}
