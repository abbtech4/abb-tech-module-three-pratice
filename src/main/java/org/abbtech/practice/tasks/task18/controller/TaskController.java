package org.abbtech.practice.tasks.task18.controller;


import org.abbtech.practice.tasks.task18.dto.TaskDto;
import org.abbtech.practice.tasks.task18.service.TaskService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/api/task")
public class TaskController {
    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<TaskDto> getTaskById(@PathVariable(value = "id") Long id){
        return new ResponseEntity<>(taskService.getTaskById(id),HttpStatus.OK);
    }

    @GetMapping("/get/all")
    public ResponseEntity<List<TaskDto>> getAllTasks(){
        return new ResponseEntity<>(taskService.getAllTasks(), HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<String> saveTask(@RequestBody TaskDto taskDto){
        taskService.saveTask(taskDto);
        return new ResponseEntity<>("Task Successfully added",HttpStatus.CREATED);
    }
    @PostMapping("/add")
    public String postTasks(@RequestBody List<TaskDto> taskDtos){
       return taskService.createTask(taskDtos);
    };
    @PatchMapping ("/update/{id}")
    public ResponseEntity<String> updateTask(@PathVariable(value = "id") Long id,@RequestBody TaskDto taskDto){
        taskService.updateTask(id,taskDto);
        return new ResponseEntity<>("Task with id of "+id+" is successfully updated",HttpStatus.CREATED);
    }



    @DeleteMapping("/delete/multiple")
    public ResponseEntity<String> deleteMultipleTasks(@RequestBody List<Long> ids){
        taskService.deleteMultipleTasks(ids);
        return new ResponseEntity<>("tasks with ids "+ ids +" are successfully deleted",HttpStatus.ACCEPTED);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteTask(@PathVariable Long id){
        taskService.deleteTask(id);
        return new ResponseEntity<>("Task with id of "+id+" is successfully deleted",HttpStatus.OK);
    }
}
