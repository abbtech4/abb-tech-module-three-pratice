package org.abbtech.practice.tasks.task18.exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GeneralExceptionHandler {

    @ExceptionHandler(NoSuchTaskException.class)
    public ResponseEntity<String> handle(NoSuchTaskException noSuchTaskException){
        return new ResponseEntity<>(noSuchTaskException.getMessage(),HttpStatus.NOT_FOUND);
    }
}
