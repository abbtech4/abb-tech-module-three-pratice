package org.abbtech.practice.tasks.task18.repository;


import org.abbtech.practice.tasks.task18.dto.TaskDto;
import org.abbtech.practice.tasks.task18.exception.NoSuchTaskException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
public class TaskRepository {
 private final JdbcTemplate jdbcTemplate;

    public TaskRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Optional<List<TaskDto>> getAllTasks() {
        List<TaskDto> taskDtos = jdbcTemplate.query("""
            SELECT * FROM TASKS
            """, (rs, rowNum) -> new TaskDto(rs.getLong("id"), rs.getString("task_name")));

        if (taskDtos.size()==0){
           return Optional.empty();
        }

        return Optional.of(taskDtos);
    }


    public void createTask(List<TaskDto> taskDto){

        jdbcTemplate.batchUpdate("INSERT INTO TASKS(task_name) VALUES (?)", new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, taskDto.get(i).getTaskName());
            }

            @Override
            public int getBatchSize() {
                return taskDto.size();
            }
        });
    }

    public Optional<TaskDto> getTaskById(Long id){
        try{
            Optional<TaskDto> taskDto = Optional.ofNullable(jdbcTemplate.queryForObject("""
                    Select * FROM TASKS WHERE id=?
                    """, (a, b) -> new TaskDto(a.getLong("id"), a.getString("task_name")), id));
            return taskDto;
        }catch(Exception e){
            return Optional.empty();
        }
    }

    public void saveTask(TaskDto taskDto){
        jdbcTemplate.update("""
        INSERT INTO TASKS(task_name) VALUES (?)
        """,taskDto.getTaskName());
    }

    public void updateTask(Long id, TaskDto taskDto){
        if (getTaskById(id).isPresent()){
            jdbcTemplate.update("""
            UPDATE Tasks set task_name=? where id=?
            """,taskDto.getTaskName(),id);
        }else {
            throw new NoSuchTaskException("No task has found");
        }


    }

    public void deleteMultipleTasks(List<Long> ids) {
        if (ids.stream().allMatch(x->getTaskById(x).isPresent())){
            ids.stream().forEach(id->jdbcTemplate.update("""
             DELETE FROM TASks where id=?
             """,id));
        }else {
            throw new NoSuchTaskException("No task(s) with provided ids");
        }

    }

    public void deleteTask(Long id){
        if (getTaskById(id).isPresent()){
            jdbcTemplate.update("""
        DELETE from Tasks where id=?
        """,id);
        }else throw new NoSuchTaskException("No task with "+id);

    }

}
