package org.abbtech.practice.tasks.task18.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoSuchTaskException extends RuntimeException{
    public NoSuchTaskException(String message){
        super((message));
    }
}
