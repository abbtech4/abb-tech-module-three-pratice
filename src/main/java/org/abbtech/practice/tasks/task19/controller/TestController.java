package org.abbtech.practice.tasks.task19.controller;

import lombok.RequiredArgsConstructor;
import org.abbtech.practice.tasks.task19.model.Student;
import org.abbtech.practice.tasks.task19.service.TestService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class TestController {
    private final TestService testService;
    @PostMapping("/post")
    public void postTest(@RequestBody Student student){
        testService.testPost(student);

    }
}
