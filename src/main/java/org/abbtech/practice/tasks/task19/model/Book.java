package org.abbtech.practice.tasks.task19.model;

import jakarta.persistence.*;
import lombok.*;

@Data
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long bookId;
    @Column(name = "title")
    private String title;
    @Column(name = "author",length = 100)
    private String author;
    @OneToOne(mappedBy = "book")
    private Professor professor;
}
