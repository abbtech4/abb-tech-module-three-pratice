package org.abbtech.practice.tasks.task19.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "Class")
@Getter
@Data
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Class {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long classId;


    @Column(name = "ClassName",length = 100)
    private String className;

    @ManyToMany(mappedBy = "classes")
    private List<Student> students;
}
