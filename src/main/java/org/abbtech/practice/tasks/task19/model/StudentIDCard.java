package org.abbtech.practice.tasks.task19.model;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Entity
@Table(name = "StudentIDCard")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class StudentIDCard {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long cardId;

    @Column(name = "CardNumber",length = 20)
    private String cardNumber;

    @Column(name = "ExpiryDate")
    private Date expiryDate;





}
