package org.abbtech.practice.tasks.task19.repository;

import org.abbtech.practice.tasks.task19.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student,Long> {
}
