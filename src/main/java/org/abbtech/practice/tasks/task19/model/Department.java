package org.abbtech.practice.tasks.task19.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long departmentId;

    @Column(name = "DepartmentName",length = 100)
    private String departmentName;

    @OneToMany(mappedBy = "department")
    private List<Professor> professor;
}
