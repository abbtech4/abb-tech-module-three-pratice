package org.abbtech.practice.tasks.task19.model;

import jakarta.persistence.*;
import lombok.*;

@Data
@Entity
@Table(name = "Professor")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Professor {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long professorId;
    @Column(name = "FirstName",length = 50)
    private String firstName;


    @Column(name = "LastName",length = 50)
    private String lastName;

    @ManyToOne
    @JoinColumn(name = "departmentId")
    private Department department;

    @OneToOne
    @JoinColumn(name = "bookId")
    private Book book;
}
