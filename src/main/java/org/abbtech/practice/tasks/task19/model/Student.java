package org.abbtech.practice.tasks.task19.model;


import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "Student")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Student {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "StudentId")
    private long studentId;
    @Column(name = "FirstName",length = 50)
    private String firstName;

    @Column(name = "LastName",length = 50)
    private String lastName;

    @OneToOne
    @JoinColumn(name = "CardID",unique = true)
    private StudentIDCard studentIDCard;

    @ManyToMany
    @JoinTable(name = "StudentClass",joinColumns = {@JoinColumn(name = "studentId",referencedColumnName = "studentId")}
            ,inverseJoinColumns =@JoinColumn(name = "classId",referencedColumnName = "classId"))
    private List<Class> classes;
}
