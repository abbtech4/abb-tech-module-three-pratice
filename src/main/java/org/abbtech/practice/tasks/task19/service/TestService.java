package org.abbtech.practice.tasks.task19.service;

import lombok.RequiredArgsConstructor;
import org.abbtech.practice.tasks.task19.model.Student;
import org.abbtech.practice.tasks.task19.repository.StudentRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TestService {
    private final StudentRepository studentRepository;

    public void testPost(Student student){
        studentRepository.save(student);
    }
}
